import React, { Component } from "react";
import {Link} from "react-router-dom";
import {Text, Heading, Box} from "rebass";

const Divider = props => (
  <Box
    {...props}
    as='hr'
    bg='gray'
    css={{
      border: 0,
      height: 1
    }}
  />
);

const entryLink = (entry) => (<Box key={entry['_']['#']}>
                                <Link to={"/" + entry.kind + "/" + entry['_']['#']}>
                                  {entry.title||entry.location}
                                </Link>
                              </Box>);
const renderSection = (section) => (
    <Box key={'section-' + section[0]}>
      {section[1].map(entryLink)}
      <Divider/>
    </Box>
);
// TODO: create a sidebar that has a search bar and a link to all our favorites
export const Sidebar = (props) => (
    <Box width={props.width} p={2} color='black'>
      <Text>Search...</Text>
      <Box>
        <Box>
          <Link to="/">Home</Link>
        </Box>
        <Divider/>
      </Box>
      {props.sections.map(renderSection)}
    </Box>
);
