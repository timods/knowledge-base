import React, { Component } from "react";

import {
    PdfLoader,
    PdfHighlighter,
    Tip,
    Highlight,
    Popup,
    AreaHighlight
} from "react-pdf-highlighter";

import Spinner from "./Spinner";
import Sidebar from "./Sidebar";

import type { T_Highlight, T_NewHighlight } from "react-pdf-highlighter/types";

import "./style/Pdf.css";

type T_ManuscriptHighlight = T_Highlight;

type Props = {
    soul: string
};

type State = {
    highlights: Array<T_ManuscriptHighlight>
};

const getNextId = () => String(Math.random()).slice(2);

const parseIdFromHash = () => window.location.hash.slice("#highlight-".length);

const resetHash = () => {
    window.location.hash = "";
};

const HighlightPopup = ({ comment }) =>
      comment.text ? (
          <div className="Highlight__popup">
            {comment.emoji} {comment.text}
          </div>
      ) : null;

const DEFAULT_URL = "https://arxiv.org/pdf/1708.08021.pdf";

class PdfContent extends Component<Props, State> {
    state = {
        // TODO: get the location from the passed in soul
        url: this.props.doc.location,
        highlights: this.props.highlights
    };

    state: State;

    resetHighlights = () => {
        this.setState({
            highlights: []
        });
    };

    scrollViewerTo = (highlight: any) => {};

    scrollToHighlightFromHash = () => {
        const highlight = this.getHighlightById(parseIdFromHash());

        if (highlight) {
            this.scrollViewerTo(highlight);
        }
    };

    componentDidUpdate(prevProps, prevState) {
        if (this.props.doc.location !== this.state.url) {
            this.setState({url: this.props.doc.location,
                           hightlights: this.props.highlights});
        }
    }

    componentDidMount() {
        window.addEventListener(
            "hashchange",
            this.scrollToHighlightFromHash,
            false
        );
    }

    getHighlightById(id: string) {
        const highlights = this.state.highlights;

        return highlights.find(highlight => highlight['_']['#'] === id);
    }

    addHighlight(highlight: T_NewHighlight) {
        this.props.addHighlight(highlight);
    }

    updateHighlight(highlightId: string, position: Object, content: Object) {
        console.log("Updating highlight", highlightId, position, content);
        console.log("TODO: test updateHightlight!");
        this.props.updateHighlight(highlightId, position, content);
    }

    render() {
        const highlights = this.state.highlights;
        // TODO: have the sidebar listen to gun.get('highlights-parentsoul')?
        //       then rewrite itself as they change (so we don't re-render the whole pdf)
        return (
            <div className="App" style={{ display: "flex", height: "95vh" }}>
              <Sidebar
                key={'pdf-sidebar-' + this.state.url}
                highlights={highlights}
                resetHighlights={this.resetHighlights}
              />
              <div
                style={{
                    height: "95vh",
                    width: "75vw",
                    overflowY: "scroll",
                    position: "relative"
                }}
              >
                <PdfLoader key={'pdfloader-' + this.state.url} url={this.state.url} beforeLoad={<Spinner />}>
                  {pdfDocument => (
                      <PdfHighlighter
                        pdfDocument={pdfDocument}
                        enableAreaSelection={event => event.altKey}
                        onScrollChange={resetHash}
                        scrollRef={scrollTo => {
                            this.scrollViewerTo = scrollTo;

                            this.scrollToHighlightFromHash();
                        }}
                        onSelectionFinished={(
                            position,
                            content,
                            hideTipAndSelection,
                            transformSelection
                        ) => (
                            <Tip
                              onOpen={transformSelection}
                              onConfirm={comment => {
                                  this.addHighlight({ content, position, comment });

                                  hideTipAndSelection();
                              }}
                            />
                        )}
                        highlightTransform={(
                            highlight,
                            index,
                            setTip,
                            hideTip,
                            viewportToScaled,
                            screenshot,
                            isScrolledTo
                        ) => {
                            const isTextHighlight = !Boolean(
                                highlight.content && highlight.content.image
                            );

                            const component = isTextHighlight ? (
                                <Highlight
                                  isScrolledTo={isScrolledTo}
                                  position={highlight.position}
                                  comment={highlight.comment}
                                />
                            ) : (
                                <AreaHighlight
                                  highlight={highlight}
                                  onChange={boundingRect => {
                                      this.updateHighlight(
                                          highlight['_']['#'],
                                          { boundingRect: viewportToScaled(boundingRect) },
                                          { image: screenshot(boundingRect) }
                                      );
                                  }}
                                />
                            );

                            return (
                                <Popup
                                  popupContent={<HighlightPopup {...highlight} />}
                                  onMouseOver={popupContent =>
                                               setTip(highlight, highlight => popupContent)
                                              }
                                  onMouseOut={hideTip}
                                  key={index}
                                  children={component}
                                />
                            );
                        }}
                        highlights={highlights}
                      />
                  )}
                </PdfLoader>
              </div>
            </div>
        );
    }
}

export default PdfContent;
