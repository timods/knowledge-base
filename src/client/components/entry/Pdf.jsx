import * as React from "react";
import Spinner from "./pdf/Spinner";
import PdfContent from "./pdf/Pdf";
import {array2object, flattenObj, unflattenObj} from '../../util';

type GunProps = {
    soul: string,
    doc: object
};

type Props = {
    gun: object,
    soul: string,
    elem: React.ComponentType<GunProps>
};

type State = {
    soul: string,
    doc: object,
    highlights: array
};

export default class PdfEntry extends React.Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {
            soul: props.soul,
            doc: null,
            highlights: []
        };
        this.elem = props.elem;
        this.gun = props.gun;
        this.docRef = this.gun.get(this.state.soul);
        this.highlightsRef = this.gun.get('highlight/' + this.state.soul);
    }

    setupGunHandles(soul) {
        this.docRef = this.gun.get(soul);

        this.highlightsRef = this.gun.get('highlight/' + soul);
        this.docRef.on((doc, key) => {
            this.setState({doc: _.merge(this.state.doc, doc)});
        });

        // Look for updates in our highlighted content
        let highlights = this.state.highlights;
        const self = this;
        this.highlightsRef.on((doc, key) => {
            // Filter out deleted highlights
            if (doc == null) {
                return;
            }
            var idList = _.reduce(doc['_']['>'], function(result, value, key) {
                let data = { id: key, date: value};
                self.gun.get(key).on((highlight, key) => {
                    // Filter out deleted highlights
                    if (highlight == null) {
                        return;
                    }
                    const merged = unflattenObj(_.merge(data, highlight));
                    if (merged.position.rects != undefined && !Array.isArray(merged.position.rects)) {
                        // Fix the whole "empty arrays stay that way" issue
                        merged.position.rects = [];
                    }
                    const index = _.findIndex(highlights, (o)=>{ return o.id === key; });
                    if(index >= 0) {
                        highlights[index] = merged;
                    }else{
                        highlights.push(merged);
                    }
                    self.setState({highlights: highlights});
                });
            }, []);
        });
    }

    componentWillMount() {
        // Look for updates on the PDF entry itself
        this.setupGunHandles(this.state.soul);
    }

    componentWillUnmount() {
        this.docRef.off();
        this.highlightsRef.off();
    }

    addHighlight(highlight) {
        highlight['kind'] = 'highlight';
        let gun_hl = flattenObj(highlight);
        // Not bothering with a highlight schema
        this.highlightsRef.set(this.gun.put(gun_hl));
    }

    removeHighlight(highlightSoul) {
        // TODO: verify removal logic
        this.highlightsRef.unset(highlightSoul);
        this.setState({
            highlights: highlights.filter((hl) => hl['_']['#'] !== highlightSoul)
        });
    }

    updateHighlight(highlightSoul, position, content) {
        this.state.highlights.forEach((highlight) => {
            let partial = {};
            let soul = highlight['_']['#'];
            if (soul === highlight) {
                partial.position = {...highlight.position, ...position};
                partial.content = {...highlight.content, ...content};
                // Gun handles the partial updating here
                // TODO: verify that this properly updates the position
                this.gun.get(soul).put(flattenObj(partial));
            }
        });
    }

    render() {
        if (this.state.doc !== null) {
            // TODO: pass in addHighlight, updateHighlight handlers here
            return (
                <PdfContent doc={this.state.doc}
                            highlights={this.state.highlights}
                            addHighlight={this.addHighlight.bind(this)}
                            updateHighlight={this.updateHighlight.bind(this)}
                />
            );
        } else {
            return <Spinner/>;
        }
    }
}
