import _ from 'lodash';

var entryTypes = [
    'txt',
    'pdf',
    'image',
    'highlight',
    'blob'
]
var entrySchema = {
    type: 'object',
    useDefault: true,
    properties: {
        title: {
            required: true,
            type: 'string'
        },
        links_to: {
            default: [],
            type: 'array'
        },
        linked_from: {
            default: [],
            type: 'array'
        },
        kind: {
            enum: entryTypes,
            required: true,
            type: 'string'
        }
    }
};

var schemas = {
    "txt":{
        required: true,
        type: 'object',
        useDefault: true,
        properties: _.merge(entrySchema.properties, {
            content: {
                description: "The body of the text file",
                required: true,
                type: 'string'
            }
        })
    },
    "pdf":{
        type: 'object',
        useDefault: true,
        properties: _.merge(entrySchema.properties, {
            location: {
                description: "A link to the pdf itself. Either an external https? url or a path in the webapp",
                required: true,
                type: 'string'
            },
            magnet: {
                description: "The magnet link used to download & seed the PDF",
                type: "string",
                required: true
            },
            hash: {
                description: "SHA256 hash (as a hex string) of the contents. Used for integrity checking",
                type: "string",
                required: true
            }
        })
    },
    "image":{
        type: 'object',
        useDefault: true,
        properties: _.merge(entrySchema.properties, {
            location: {
                description: "A link to the image file",
                type: "string",
                required: true
            }
        })
    },
    "blob":{
        type: 'object',
        useDefault: true,
        properties: _.merge(entrySchema.properties, {
            location: {
                description: "A link to the file blob. Either an external https? url or a path in the webapp",
                required: true,
                type: 'string'
            },
            magnet: {
                description: "The magnet link used to download & seed the file",
                type: "string",
                required: true
            },
            hash: {
                description: "SHA256 hash (as a hex string) of the blob. Used for integrity checking",
                type: "string",
                required: true
            }
        })
    }
}

export function registerSchemas(gun) {
    for (let prop in schemas) {
        if (!schemas.hasOwnProperty(prop)) {
            continue;
        }
        console.log("Registering schema for " + prop);
        gun.schema(prop, schemas[prop]);
    }
    return gun;
}
