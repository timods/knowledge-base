import Gun from 'gun';

export function unflattenObj(data) {
    "use strict";
    if (Object(data) !== data || Array.isArray(data))
        return data;
    var result = {}, cur, prop, parts, idx;
    for(var p in data) {
        cur = result, prop = "";
        parts = p.split(".");
        for(var i=0; i<parts.length; i++) {
            idx = !isNaN(parseInt(parts[i]));
            cur = cur[prop] || (cur[prop] = (idx ? [] : {}));
            prop = parts[i];
        }
        cur[prop] = data[p];
    }
    return result[""];
}
export function flattenObj(data) {
    var result = {};
    function recurse (cur, prop) {
        if (Object(cur) !== cur) {
            result[prop] = cur;
        } else if (Array.isArray(cur)) {
             for(var i=0, l=cur.length; i<l; i++)
                 recurse(cur[i], prop ? prop+"."+i : ""+i);
            if (l == 0) {
                // Empty arrays become empty objects
                result[prop] = {};
            }
        } else {
            var isEmpty = true;
            for (var p in cur) {
                isEmpty = false;
                recurse(cur[p], prop ? prop+"."+p : p);
            }
            if (isEmpty)
                result[prop] = {};
        }
    }
    recurse(data, "");
    return result;
}

export function array2object(arr){
  var obj = {};
  Gun.list.map(arr, function(v,f,t){
    if(Gun.list.is(v) || Gun.obj.is(v)){
      obj[f] = array2object(v);
      return;
    }
    obj[f] = v;
  });
  return obj;
}

// The following only works on an object that was converted using a2o above
export function object2array(obj) {
    var obj = {};
    Gun.list.map(obj, function(v,f,t) {
        if (Gun.list.is(v) || Gun.obj.is(v)) {
            obj[f] = object2array(v);
            return;
        }
        obj[f] = v;
    })
    return obj;
}
