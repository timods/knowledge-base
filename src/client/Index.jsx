import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Gun from 'gun';
import AppRouter from './Routes';

class Index extends Component {
    render() {
        return (
            <div className="App">
              <header className="App-header">
                <h1>PDF Time</h1>
              </header>
              <nav>
                <ul>
                  <li>
                    <Link to="/">Home</Link>
                  </li>
                  <li>
                    <Link to="/pdf/demo">PDF Files</Link>
                  </li>
                </ul>
              </nav>
              <AppRouter />
            </div>
        );
    }
}

export default App;
//<Pdf location={"https://arxiv.org/pdf/1805.03450.pdf"}/>
