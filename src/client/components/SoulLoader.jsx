import * as React from "react";
import Spinner from "./entry/pdf/Spinner";

type GunProps = {
    soul: string,
    doc: object
};

type Props = {
    gun: object,
    soul: string,
    elem: React.ComponentType<GunProps>
};

type State = {
    soul: string,
    doc: object
};

export default class SoulLoader extends React.Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {
            soul: props.soul,
            doc: null,
        };
        this.elem = props.elem;
        this.gun = props.gun;
    }

    componentWillMount() {
        this.gun.get(this.state.soul).on((doc, key) => {
            this.setState({doc: doc});
        });
    }

    componentWillUnmount() {
        this.gun.get(this.state.soul).off();
    }

    render() {
        if (this.state.doc !== null) {
            let Elem = this.elem;
            return (
                <Elem soul={this.state.soul} doc={this.state.doc}/>
            );
        } else {
            return <Spinner/>;
        }
    }
}

export const soulLoader = (gun, Elem) => (props) => (
    <SoulLoader
      gun={gun}
      soul={props.match.params.soul}
      elem={Elem}
      {...props}/>);
