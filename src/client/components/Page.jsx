import React from "react";
import {Box,Flex,Text,Heading} from "rebass";

export const pageWrapper = (Elem) => (props) => (
    <Box>
      <Box>
        <Heading textAlign='center' >{props.doc.title}</Heading>
      </Box>
      <Box>
        <Elem {...props}/>
      </Box>
    </Box>
);
