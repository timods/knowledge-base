const path = require('path');
const express = require('express');
const app = express();
// Gun setup
const Gun = require('gun');
require('gun/lib/unset.js');
require('gun-tag');
require("gun-schema");

const port = (process.env.PORT || 8080);

if (process.env.NODE_ENV !== 'production') {
  const webpack = require('webpack');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');
  const config = require('../../webpack.config.js');
  const compiler = webpack(config);

  app.use(webpackHotMiddleware(compiler));
  app.use(webpackDevMiddleware(compiler));
}else{
  const indexPath = path.join(__dirname, '../../build/index.html');
  app.use(express.static('dist'));
  app.get('*', function (_, res) {
    res.sendFile(indexPath);
  });
}

app.use(Gun.serve);
const server = app.listen(port);
Gun({ file: 'db/data.json', web: server });
