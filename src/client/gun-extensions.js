import registerSchemas from './entry/schemas';

export function registerExtensions(Gun) {
    console.log('loading gun');
    require('gun/lib/unset.js');
    require('gun-tag');
    return registerSchemas(Gun);
}
