import React, { Component } from 'react';
import _ from 'lodash';
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {Box, Flex, Heading} from "rebass";
import { ThemeProvider } from 'styled-components';
import theme from './theme';

// custom components
import {registerSchemas} from './components/entry/schemas';
import {Sidebar} from './components/sidebar';
import {soulLoader} from './components/SoulLoader';
import {pageWrapper} from './components/Page';
import TextEntry from './components/entry/Text';
import PdfEntry from './components/entry/Pdf';
import {Home} from './components/Home';

// GUN
import Gun from 'gun';
require('gun/lib/unset.js');
require('gun-tag');
require('gun-schema');


const ContentWrapper = (props) => {props.children};

class App extends Component {
    constructor(props) {
        super(props);
        this.gun = registerSchemas(Gun(location.origin + '/gun'));
        this.entriesRef = this.gun.get('entries');
        this.state = {entries: [], currentId: ''};
        window.gun = this.gun;
    }

    componentWillMount() {
        let entries = this.state.entries;
        const self = this;
        this.entriesRef.on((doc) => {
            var idList = _.reduce(doc['_']['>'], function(result, value, key) {
                let data = { id: key, date: value};
                self.gun.get(key).on((file, key) => {
                    const merged = _.merge(data, file);
                    const index = _.findIndex(entries, (o)=>{ return o.id === key; });
                    if(index >= 0) {
                        entries[index] = merged;
                    }else{
                        entries.push(merged);
                    }
                    self.setState({entries: entries});
                });
            }, []);
        });
    }
    render() {
        let entries = this.state.entries;
        let self = this;
        return (
            <ThemeProvider theme={theme}>
              <Router>
                <Flex color='black'>
                  <Sidebar width={1/12} sections={[['entries', entries]]}/>
                    <Box width={11/12} pl={2} pt={2}>
                      <Box >
                        <Heading textAlign='center' >Page Heading</Heading>
                      </Box>
                      <Box>
                        <Switch>
                          <Route exact path="/" component={Home} />
                          <Route path="/text/:soul"
                                 render={soulLoader(self.gun, pageWrapper(TextEntry))} />
                          <Route path="/pdf/:soul"
                                 render={(props) => <PdfEntry gun={self.gun}
                                                              key={props.match.params.soul}
                                                              soul={props.match.params.soul}
                                                              {...props}/>} />
                        </Switch>
                      </Box>
                    </Box>
                </Flex>
              </Router>
            </ThemeProvider>
        );
    }
}

export default App;
