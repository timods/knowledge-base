var webpack = require('webpack');
var path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HTMLWebpackPlugin = require('html-webpack-plugin');

var src_dir = __dirname + '/src';

var VENDOR_LIBS = ['lodash', 'react', 'react-dom' ];

var production = process.env.NODE_ENV === 'production';

webpackConfig = {
    entry: {
        bundle: './src/client/index.js',
        vendor: VENDOR_LIBS
    },
    mode: 'production',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].[hash].js',
        globalObject: 'this'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: 'babel-loader',
                include: path.resolve(__dirname, 'src/client'),
            },{
                test: /\.css$/,
                //include: path.resolve(__dirname, 'src/client'),
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    "css-loader"
                ],
            },
            {
                //IMAGE LOADER
                test: /\.(jpe?g|png|gif|svg)$/i,
                loader:'file-loader'
            },
        ]
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: 'public/index.html'
        }),
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new webpack.DefinePlugin({
            'process.env.': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV)
            }
        }),
        new webpack.HotModuleReplacementPlugin()
    ]
};

if(!production) {
    webpackConfig.devServer = {
        host: process.env.BIND || '127.0.0.1',
        port: '8080',
        headers: { 'Access-Control-Allow-Origin': '*' }
    };
    // Source maps
    webpackConfig.devtool = 'inline-source-map';
    webpackConfig.optimization = {
        removeAvailableModules: false,
        removeEmptyChunks: false,
        splitChunks: false,
    };
    webpackConfig.mode = 'development'
}

module.exports = webpackConfig;
